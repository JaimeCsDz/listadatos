export interface IPost {
    id:number;
    title: string;
    body:string;
    date: Date;
    nombre: string;
}